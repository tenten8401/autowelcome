package site.isaacs.AutoWelcome;

import java.util.Random;

import org.slf4j.Logger;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.EventContext;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.text.Text;

public class AutoWelcomeCommand implements CommandExecutor {
	 
	private AutoWelcome instance;
	private Logger logger;
	
	public AutoWelcomeCommand(AutoWelcome instance) {
		this.instance = instance;
		this.logger = this.instance.getLogger();
	}
	

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (src instanceof Player) {
			// Declare variables
			Player player = (Player) src;
			Player lastJoined = AutoWelcome.getLastPlayerJoin();
			String lastJoinedName = lastJoined.getName();
			Text msg;
			
			if(player.getName() != lastJoined.getName()) {
				logger.info(player.getName() + " used AutoWelcome");
				
				// Randomize name to make it feel like they cared enough to type it out
				int rndname = new Random().nextInt(3);
				logger.info("Random Number: " + rndname);
				switch(rndname) {
					case 0:
						lastJoinedName = lastJoinedName.toLowerCase();
						break;
					case 1:
						lastJoinedName = lastJoinedName.toLowerCase().replaceFirst("([A-z]+)([0-9]+)$", "$1");
						break;
					case 2:
						break;
				}
				
				// Choose greeting
				if((lastJoined.getJoinData().lastPlayed().get().getEpochSecond() - lastJoined.getJoinData().firstPlayed().get().getEpochSecond()) < 90) {
					// Player has not played before
					msg = Text.of(AutoWelcome.getFirstTimeGreetings()[new Random().nextInt(AutoWelcome.getFirstTimeGreetings().length)]
							.replace("%player%", lastJoinedName));
				} else {
					// Player has played before
					msg = Text.of(AutoWelcome.getGreetings()[new Random().nextInt(AutoWelcome.getGreetings().length)]
							.replace("%player%", lastJoinedName));
				}
	
				// Send Message
				EventContext context = EventContext.builder().add(EventContextKeys.PLAYER_SIMULATED, player.getProfile())
						.build();
				Cause messageCause = Cause.builder().append(player).build(context);
				player.simulateChat(msg, messageCause);
				return CommandResult.success();
			} else {
				src.sendMessage(Text.of("You cannot welcome yourself!"));
				return CommandResult.empty();
			}
		} else {
			// Something that's not a player sent it
			src.sendMessage(Text.of("This command is intended for players only."));
			return CommandResult.empty();
		}
	}

}
