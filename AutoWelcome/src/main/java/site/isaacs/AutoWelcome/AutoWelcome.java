package site.isaacs.AutoWelcome;

import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

import com.google.inject.Inject;

@Plugin(id = "autowelcome", name = "AutoWelcome", version = "1.0", description = "Allows you to welcome people to the server by typing /aw")
public class AutoWelcome {

	private static AutoWelcome aw = new AutoWelcome();

	private static Player lastPlayerJoin;
	private static String[] firstTimeGreetings = { "Welcome!", "Welcome %player%!", "Welcome to the server %player%!", "Welcome", "Welcome %player%" };
	private static String[] greetings = { "Welcome back", "Welcome back %player%", "wb", "wb %player%" };


	@Inject
	private Logger logger;
	
	@Listener
	public void onServerStart(GameStartedServerEvent event) {
		CommandSpec awcmd = CommandSpec.builder()
				.description(Text.of("Automatically welcome somebody to a server"))
				.permission("autowelcome.aw")
				.executor(new AutoWelcomeCommand(this))
				.build();
		
		Sponge.getCommandManager().register(this, awcmd, "aw", "autowelcome", "wb");
		
		logger.info("AutoWelcome Loaded!");
	}

	@Listener
	public void onPlayerJoin(ClientConnectionEvent.Join event) {
		lastPlayerJoin = event.getTargetEntity();
		logger.info("Last Player Joined: " + lastPlayerJoin.getName());
	}

	public static Player getLastPlayerJoin() {
		return lastPlayerJoin;
	}
	
	public static String[] getFirstTimeGreetings() {
		return firstTimeGreetings;
	}
	
	public static String[] getGreetings() {
		return greetings;
	}
	
	public Logger getLogger() {
		return logger;
	}

	public static AutoWelcome getInstance() {
		return aw;
	}
}
